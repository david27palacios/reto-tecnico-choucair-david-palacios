package co.com.choucair.certification.retotecnicochoucair.tasks;


import co.com.choucair.certification.retotecnicochoucair.userinterface.LoginPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;



public class Login implements Task {
    private String email;
    private String password;
    public Login(String email,String password) {

        this.email = email;
        this.password = password;
    }

    public static Login the(String email, String password) {
        return Tasks.instrumented(Login.class,email,password);
    }



    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(LoginPage.BUTTON_LOGIN),
                Enter.theValue(email).into(LoginPage.INPUT_EMAIL),
                Enter.theValue(password).into(LoginPage.INPUT_PASSWORD),
                Click.on(LoginPage.BUTTON_SUMBIT)
        );
    }
}
