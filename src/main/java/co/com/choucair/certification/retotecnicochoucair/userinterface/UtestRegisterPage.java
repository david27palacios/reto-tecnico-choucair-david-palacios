package co.com.choucair.certification.retotecnicochoucair.userinterface;


import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;


public class UtestRegisterPage {
    public static final Target REGISTER_BUTTON = Target.the("button that shows us the form to SING UP").located(By.xpath("/html/body/ui-view/unauthenticated-container/div/div/unauthenticated-header/div/div[3]/ul[2]/li[2]/a"));
    public static final Target INPUT_FIRSTNAME = Target.the("where do we write the first Name").located(By.id("firstName"));
    public static final Target INPUT_LASTNAME = Target.the("where do we write the Last name").located(By.id("lastName"));
    public static final Target INPUT_EMAIL = Target.the("where do we write the email").located(By.id("email"));
    public static final Target INPUT_LANGUAGES = Target.the("where do we write the password").located(By.xpath("//*[@id=\"languages\"]/div[1]/input"));
    public static final Target SELECT_MONTH = Target.the("where do we write the birth month").located(By.id("birthMonth"));
    public static final Target SELECT_DAY = Target.the("where do we write the day birth").located(By.id("birthDay"));
    public static final Target SELECT_YEAR = Target.the("where do we write the year birth").located(By.id("birthYear"));
    public static final Target CONFIRM_BUTTON1 = Target.the("button to confirm the information").located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[2]/a"));
    public static final Target INPUT_CITY = Target.the("where do we write the city of residence").located(By.id("city"));
    public static final Target INPUT_CODPOSTAL = Target.the("where do we write our zip").located(By.id("zip"));
    public static final Target CONFIRM_BUTTON2 = Target.the("button to confirm the information").located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[2]/div/a"));
    public static final Target SPAN_MOBILE = Target.the("where do we active the input about our brand of mobile").located(By.xpath("//*[@id=\"mobile-device\"]/div[1]/div[2]/div/div[1]/span/span[1]"));
    public static final Target SPAN_MOBILEMODEL = Target.the("where do we active the input about  our brand of mobile").located(By.xpath("//*[@id=\"mobile-device\"]/div[2]/div[2]/div/div[1]/span"));
    public static final Target SPAN_MOBILESYSTEM = Target.the("where do we active the input about  our brand of mobile").located(By.xpath("//*[@id=\"mobile-device\"]/div[3]/div[2]/div/div[1]/span"));
    public static final Target INPUT_MOBILE = Target.the("where do we write our brand of mobile").located(By.xpath("//*[@id=\"mobile-device\"]/div[1]/div[2]/div/input[1]"));
    public static final Target INPUT_MOBILEMODEL = Target.the("where do we write the model of your mobile").located(By.xpath("//*[@id=\"mobile-device\"]/div[2]/div[2]/div/input[1]"));
    public static final Target INPUT_MOBILESYSTEM = Target.the("where do we write the system of your mobile").located(By.xpath("//*[@id=\"mobile-device\"]/div[3]/div[2]/div/input[1]"));
    public static final Target CONFIRM_BUTTON3 = Target.the("button to confirm the information").located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/div[2]/div/a"));
    public static final Target INPUT_PASSWORD = Target.the("where do we write our password").located(By.id("password"));
    public static final Target INPUT_CONFIRMPASSWORD = Target.the("where do we confirm our password").located(By.id("confirmPassword"));
    public static final Target CHECK_TERMS = Target.the("where do we accept terms").located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[5]/label/span[1]"));
    public static final Target CHECK_PRIVACY = Target.the("where do we accept privacy").located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[6]/label/span[1]"));
    public static final Target SUMBMIT_FORM = Target.the("where do we submit form").located(By.xpath("//*[@id=\"laddaBtn\"]"));

        }