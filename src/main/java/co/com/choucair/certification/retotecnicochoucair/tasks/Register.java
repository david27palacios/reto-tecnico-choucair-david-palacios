package co.com.choucair.certification.retotecnicochoucair.tasks;



import co.com.choucair.certification.retotecnicochoucair.userinterface.UtestPage;
import co.com.choucair.certification.retotecnicochoucair.userinterface.UtestRegisterPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import org.openqa.selenium.Keys;

public class Register implements Task {
    private String firstName;
    private String lastName;
    private String email;
    private String month;
    private String day;
    private String year;
    private String city;
    private String zip;
    private String mobile;
    private String model;
    private String systemOperative;
    private String password;

    public Register(String firstName, String lastName, String email, String month, String day, String year, String city, String zip, String mobile, String model, String systemOperative, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.month = month;
        this.day = day;
        this.year = year;
        this.city = city;
        this.zip = zip;
        this.mobile = mobile;
        this.model = model;
        this.systemOperative = systemOperative;
        this.password = password;
    }


    public static Register OnThePage(String firstName, String lastName, String email, String month, String day, String year, String city, String zip, String mobile, String model, String systemOperative, String password) {
        return Tasks.instrumented(Register.class,firstName,lastName,email,month,day,year,city,zip,mobile,model,systemOperative,password);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(UtestRegisterPage.REGISTER_BUTTON),
                Enter.theValue(firstName).into(UtestRegisterPage.INPUT_FIRSTNAME),
                Enter.theValue(lastName).into(UtestRegisterPage.INPUT_LASTNAME),
                Enter.theValue(email).into(UtestRegisterPage.INPUT_EMAIL),
                Enter.theValue("Spanish").into(UtestRegisterPage.INPUT_LANGUAGES),
                Hit.the(Keys.ENTER).into(UtestRegisterPage.INPUT_LANGUAGES),
                SelectFromOptions.byVisibleText(month).from(UtestRegisterPage.SELECT_MONTH),
                SelectFromOptions.byVisibleText(day).from(UtestRegisterPage.SELECT_DAY),
                SelectFromOptions.byVisibleText(year).from(UtestRegisterPage.SELECT_YEAR),
                Click.on(UtestRegisterPage.CONFIRM_BUTTON1),
                Enter.theValue(city).into(UtestRegisterPage.INPUT_CITY),
                Enter.theValue(zip).into(UtestRegisterPage.INPUT_CODPOSTAL),
                Click.on(UtestRegisterPage.CONFIRM_BUTTON2),
                Click.on(UtestRegisterPage.SPAN_MOBILE),
                Enter.theValue(mobile).into(UtestRegisterPage.INPUT_MOBILE),
                Hit.the(Keys.ENTER).into(UtestRegisterPage.INPUT_MOBILE),
                Click.on(UtestRegisterPage.SPAN_MOBILEMODEL),
                Enter.theValue(model).into(UtestRegisterPage.INPUT_MOBILEMODEL),
                Hit.the(Keys.ENTER).into(UtestRegisterPage.INPUT_MOBILEMODEL),
                Click.on(UtestRegisterPage.SPAN_MOBILESYSTEM),
                Enter.theValue(systemOperative).into(UtestRegisterPage.INPUT_MOBILESYSTEM),
                Hit.the(Keys.ENTER).into(UtestRegisterPage.INPUT_MOBILESYSTEM),
                Click.on(UtestRegisterPage.CONFIRM_BUTTON3),
                Enter.theValue(password).into(UtestRegisterPage.INPUT_PASSWORD),
                Enter.theValue(password).into(UtestRegisterPage.INPUT_CONFIRMPASSWORD),
                Click.on(UtestRegisterPage.CHECK_PRIVACY),
                Click.on(UtestRegisterPage.CHECK_TERMS),
                Click.on(UtestRegisterPage.SUMBMIT_FORM)
                );
    }
}
