package co.com.choucair.certification.retotecnicochoucair.stepdefinitions;


import co.com.choucair.certification.retotecnicochoucair.model.UtestData;
import co.com.choucair.certification.retotecnicochoucair.questions.Answer;
import co.com.choucair.certification.retotecnicochoucair.tasks.Login;
import co.com.choucair.certification.retotecnicochoucair.tasks.Register;
import co.com.choucair.certification.retotecnicochoucair.tasks.OpenUp;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;


public class UtestStepDefinitions {


    @Before
    public void setStage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^: than David wants to register in Utest$")
    public void thanDavidWantsToRegisterInUtest() {
        OnStage.theActorCalled("David").wasAbleTo(OpenUp.thePage());
    }

    @When("^: he enters the requerid information$")
    public void heEntersTheRequeridInformation(List<UtestData> utestData) throws Exception {
        OnStage.theActorInTheSpotlight().attemptsTo(Register.OnThePage(utestData.get(0).getFirstName(),utestData.get(0).getLastName(),utestData.get(0).getEmail(),utestData.get(0).getMonth(),
                utestData.get(0).getDay(),utestData.get(0).getYear(),utestData.get(0).getCity(),utestData.get(0).getZip(),utestData.get(0).getMobile(),utestData.get(0).getModel(),
                utestData.get(0).getSystemOperative(),utestData.get(0).getPassword()), Login.the(utestData.get(0).getEmail(),utestData.get(0).getPassword()));
    }
    @Then("^: he registers in the system$")
    public void heRegistersInTheSystem(List<UtestData> utestData) throws Exception {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.toThe(utestData.get(0).getEmail())));
    }
}